﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using jjonesSite.Models;

namespace jjonesSite.Controllers
{
    public class PublisherContactController : Controller
    {
        private jjonesEEntities db = new jjonesEEntities();

        // GET: PublisherContact
        public ActionResult Index()
        {
            var pUBLISHER_CONTACT = db.PUBLISHER_CONTACT.Include(p => p.PUBLISHER);
            return View(pUBLISHER_CONTACT.ToList());
        }

        // GET: PublisherContact/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PUBLISHER_CONTACT pUBLISHER_CONTACT = db.PUBLISHER_CONTACT.Find(id);
            if (pUBLISHER_CONTACT == null)
            {
                return HttpNotFound();
            }
            return View(pUBLISHER_CONTACT);
        }

        // GET: PublisherContact/Create
        public ActionResult Create()
        {
            ViewBag.PublisherID = new SelectList(db.PUBLISHERs, "PublisherID", "PublisherName");
            return View();
        }

        // POST: PublisherContact/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ContactID,PublisherID,LastName,FirstName,Extension,Email")] PUBLISHER_CONTACT pUBLISHER_CONTACT)
        {
            if (ModelState.IsValid)
            {
                db.PUBLISHER_CONTACT.Add(pUBLISHER_CONTACT);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.PublisherID = new SelectList(db.PUBLISHERs, "PublisherID", "PublisherName", pUBLISHER_CONTACT.PublisherID);
            return View(pUBLISHER_CONTACT);
        }

        // GET: PublisherContact/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PUBLISHER_CONTACT pUBLISHER_CONTACT = db.PUBLISHER_CONTACT.Find(id);
            if (pUBLISHER_CONTACT == null)
            {
                return HttpNotFound();
            }
            ViewBag.PublisherID = new SelectList(db.PUBLISHERs, "PublisherID", "PublisherName", pUBLISHER_CONTACT.PublisherID);
            return View(pUBLISHER_CONTACT);
        }

        // POST: PublisherContact/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ContactID,PublisherID,LastName,FirstName,Extension,Email")] PUBLISHER_CONTACT pUBLISHER_CONTACT)
        {
            if (ModelState.IsValid)
            {
                db.Entry(pUBLISHER_CONTACT).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.PublisherID = new SelectList(db.PUBLISHERs, "PublisherID", "PublisherName", pUBLISHER_CONTACT.PublisherID);
            return View(pUBLISHER_CONTACT);
        }

        // GET: PublisherContact/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PUBLISHER_CONTACT pUBLISHER_CONTACT = db.PUBLISHER_CONTACT.Find(id);
            if (pUBLISHER_CONTACT == null)
            {
                return HttpNotFound();
            }
            return View(pUBLISHER_CONTACT);
        }

        // POST: PublisherContact/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            PUBLISHER_CONTACT pUBLISHER_CONTACT = db.PUBLISHER_CONTACT.Find(id);
            db.PUBLISHER_CONTACT.Remove(pUBLISHER_CONTACT);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
