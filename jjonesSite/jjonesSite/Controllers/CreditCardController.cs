﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using jjonesSite.Models;

namespace jjonesSite.Controllers
{
    public class CreditCardController : Controller
    {
        private jjonesEEntities db = new jjonesEEntities();

        // GET: CreditCard
        public ActionResult Index()
        {
            var cREDIT_CARD = db.CREDIT_CARD.Include(c => c.CUSTOMER);
            return View(cREDIT_CARD.ToList());
        }

        // GET: CreditCard/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CREDIT_CARD cREDIT_CARD = db.CREDIT_CARD.Find(id);
            if (cREDIT_CARD == null)
            {
                return HttpNotFound();
            }
            return View(cREDIT_CARD);
        }

        // GET: CreditCard/Create
        public ActionResult Create()
        {
            ViewBag.CustomerID = new SelectList(db.CUSTOMERs, "CustomerID", "Email");
            return View();
        }

        // POST: CreditCard/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "CardID,CustomerID,CardNumber,ExpirationDate,CVV,BillingAddress")] CREDIT_CARD cREDIT_CARD)
        {
            if (ModelState.IsValid)
            {
                db.CREDIT_CARD.Add(cREDIT_CARD);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.CustomerID = new SelectList(db.CUSTOMERs, "CustomerID", "Email", cREDIT_CARD.CustomerID);
            return View(cREDIT_CARD);
        }

        // GET: CreditCard/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CREDIT_CARD cREDIT_CARD = db.CREDIT_CARD.Find(id);
            if (cREDIT_CARD == null)
            {
                return HttpNotFound();
            }
            ViewBag.CustomerID = new SelectList(db.CUSTOMERs, "CustomerID", "Email", cREDIT_CARD.CustomerID);
            return View(cREDIT_CARD);
        }

        // POST: CreditCard/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "CardID,CustomerID,CardNumber,ExpirationDate,CVV,BillingAddress")] CREDIT_CARD cREDIT_CARD)
        {
            if (ModelState.IsValid)
            {
                db.Entry(cREDIT_CARD).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.CustomerID = new SelectList(db.CUSTOMERs, "CustomerID", "Email", cREDIT_CARD.CustomerID);
            return View(cREDIT_CARD);
        }

        // GET: CreditCard/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CREDIT_CARD cREDIT_CARD = db.CREDIT_CARD.Find(id);
            if (cREDIT_CARD == null)
            {
                return HttpNotFound();
            }
            return View(cREDIT_CARD);
        }

        // POST: CreditCard/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            CREDIT_CARD cREDIT_CARD = db.CREDIT_CARD.Find(id);
            db.CREDIT_CARD.Remove(cREDIT_CARD);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
