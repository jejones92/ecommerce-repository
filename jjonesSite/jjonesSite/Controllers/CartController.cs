﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using jjonesSite.Models;

namespace jjonesSite.Controllers
{
    public class CartController : Controller
    {
        private jjonesEEntities db = new jjonesEEntities();
        
        // GET: Cart
        public ActionResult Index()
        {
            var cARTs = db.CARTs.Include(c => c.BOOK).Include(c => c.CUSTOMER);
            return View(cARTs.ToList());
        }

        // GET: Cart/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CART cART = db.CARTs.Find(id);
            if (cART == null)
            {
                return HttpNotFound();
            }
            return View(cART);
        }
        public ActionResult AddToCart(int id)
        {
            //ViewBag.BookID = new SelectList(db.BOOKs, "BookID", "Title");
            //ViewBag.CustomerID = new SelectList(db.CUSTOMERs, "CustomerID", "Email");
            //return View();
            // Retrieve the album from the database
            //var addedBook = db.BOOKs.Single(book => book.BookID == id);
            // Add it to the shopping cart
            db.AddToCart(id);
            db.SaveChanges();
            // Go back to the main store page for more shopping
            return RedirectToAction("Index");
        }

        public ActionResult UpdateCart(int cartId, int quantity)
        {
            //ViewBag.BookID = new SelectList(db.BOOKs, "BookID", "Title");
            //ViewBag.CustomerID = new SelectList(db.CUSTOMERs, "CustomerID", "Email");
            //return View();
            // Retrieve the album from the database
            //var addedBook = db.BOOKs.Single(book => book.BookID == id);
            // Add it to the shopping cart
            db.UpdateCart(cartId, quantity);
            db.SaveChanges();
            // Go back to the main store page for more shopping
            return RedirectToAction("Index");
        }

        // GET: Cart/Create
        public ActionResult Create()
        {
            ViewBag.BookID = new SelectList(db.BOOKs, "BookID", "Title");
            ViewBag.CustomerID = new SelectList(db.CUSTOMERs, "CustomerID", "Email");
            return View();
        }

        // POST: Cart/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "CartID,BookID,Quantity,CustomerID,Cost,Price,History")] CART cART)
        {
            if (ModelState.IsValid)
            {
                db.CARTs.Add(cART);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.BookID = new SelectList(db.BOOKs, "BookID", "Title", cART.BookID);
            ViewBag.CustomerID = new SelectList(db.CUSTOMERs, "CustomerID", "Email", cART.CustomerID);
            return View(cART);
        }

        // GET: Cart/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CART cART = db.CARTs.Find(id);
            if (cART == null)
            {
                return HttpNotFound();
            }
            ViewBag.BookID = new SelectList(db.BOOKs, "BookID", "Title", cART.BookID);
            ViewBag.CustomerID = new SelectList(db.CUSTOMERs, "CustomerID", "Email", cART.CustomerID);
            return View(cART);
        }

        // POST: Cart/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "CartID,BookID,Quantity,CustomerID,Cost,Price,History")] CART cART)
        {
            if (ModelState.IsValid)
            {
                db.Entry(cART).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.BookID = new SelectList(db.BOOKs, "BookID", "Title", cART.BookID);
            ViewBag.CustomerID = new SelectList(db.CUSTOMERs, "CustomerID", "Email", cART.CustomerID);
            return View(cART);
        }

        // GET: Cart/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CART cART = db.CARTs.Find(id);
            if (cART == null)
            {
                return HttpNotFound();
            }
            return View(cART);
        }

        // POST: Cart/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            CART cART = db.CARTs.Find(id);
            db.CARTs.Remove(cART);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
