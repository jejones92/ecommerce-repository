﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using jjonesSite.Models;

namespace jjonesSite.Controllers
{
    public class InvoiceBookController : Controller
    {
        private jjonesEEntities db = new jjonesEEntities();

        // GET: InvoiceBook
        public ActionResult Index()
        {
            var iNVOICE_BOOK = db.INVOICE_BOOK.Include(i => i.BOOK).Include(i => i.INVOICE);
            return View(iNVOICE_BOOK.ToList());
        }

        // GET: InvoiceBook/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            INVOICE_BOOK iNVOICE_BOOK = db.INVOICE_BOOK.Find(id);
            if (iNVOICE_BOOK == null)
            {
                return HttpNotFound();
            }
            return View(iNVOICE_BOOK);
        }

        // GET: InvoiceBook/Create
        public ActionResult Create()
        {
            ViewBag.BookID = new SelectList(db.BOOKs, "BookID", "Title");
            ViewBag.InvoiceID = new SelectList(db.INVOICEs, "InvoiceID", "InvoiceID");
            return View();
        }

        // POST: InvoiceBook/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "InvoiceBookID,InvoiceID,BookID")] INVOICE_BOOK iNVOICE_BOOK)
        {
            if (ModelState.IsValid)
            {
                db.INVOICE_BOOK.Add(iNVOICE_BOOK);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.BookID = new SelectList(db.BOOKs, "BookID", "Title", iNVOICE_BOOK.BookID);
            ViewBag.InvoiceID = new SelectList(db.INVOICEs, "InvoiceID", "InvoiceID", iNVOICE_BOOK.InvoiceID);
            return View(iNVOICE_BOOK);
        }

        // GET: InvoiceBook/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            INVOICE_BOOK iNVOICE_BOOK = db.INVOICE_BOOK.Find(id);
            if (iNVOICE_BOOK == null)
            {
                return HttpNotFound();
            }
            ViewBag.BookID = new SelectList(db.BOOKs, "BookID", "Title", iNVOICE_BOOK.BookID);
            ViewBag.InvoiceID = new SelectList(db.INVOICEs, "InvoiceID", "InvoiceID", iNVOICE_BOOK.InvoiceID);
            return View(iNVOICE_BOOK);
        }

        // POST: InvoiceBook/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "InvoiceBookID,InvoiceID,BookID")] INVOICE_BOOK iNVOICE_BOOK)
        {
            if (ModelState.IsValid)
            {
                db.Entry(iNVOICE_BOOK).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.BookID = new SelectList(db.BOOKs, "BookID", "Title", iNVOICE_BOOK.BookID);
            ViewBag.InvoiceID = new SelectList(db.INVOICEs, "InvoiceID", "InvoiceID", iNVOICE_BOOK.InvoiceID);
            return View(iNVOICE_BOOK);
        }

        // GET: InvoiceBook/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            INVOICE_BOOK iNVOICE_BOOK = db.INVOICE_BOOK.Find(id);
            if (iNVOICE_BOOK == null)
            {
                return HttpNotFound();
            }
            return View(iNVOICE_BOOK);
        }

        // POST: InvoiceBook/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            INVOICE_BOOK iNVOICE_BOOK = db.INVOICE_BOOK.Find(id);
            db.INVOICE_BOOK.Remove(iNVOICE_BOOK);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
