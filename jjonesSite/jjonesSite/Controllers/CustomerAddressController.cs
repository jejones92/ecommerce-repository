﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using jjonesSite.Models;

namespace jjonesSite.Controllers
{
    public class CustomerAddressController : Controller
    {
        private jjonesEEntities db = new jjonesEEntities();

        // GET: CustomerAddress
        public ActionResult Index()
        {
            var cUSTOMER_ADDRESS = db.CUSTOMER_ADDRESS.Include(c => c.CUSTOMER);
            return View(cUSTOMER_ADDRESS.ToList());
        }

        // GET: CustomerAddress/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CUSTOMER_ADDRESS cUSTOMER_ADDRESS = db.CUSTOMER_ADDRESS.Find(id);
            if (cUSTOMER_ADDRESS == null)
            {
                return HttpNotFound();
            }
            return View(cUSTOMER_ADDRESS);
        }

        // GET: CustomerAddress/Create
        public ActionResult Create()
        {
            ViewBag.CustomerID = new SelectList(db.CUSTOMERs, "CustomerID", "Email");
            return View();
        }

        // POST: CustomerAddress/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "CustomerID,AddressID,Street,City,State,Zipcode")] CUSTOMER_ADDRESS cUSTOMER_ADDRESS)
        {
            if (ModelState.IsValid)
            {
                db.CUSTOMER_ADDRESS.Add(cUSTOMER_ADDRESS);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.CustomerID = new SelectList(db.CUSTOMERs, "CustomerID", "Email", cUSTOMER_ADDRESS.CustomerID);
            return View(cUSTOMER_ADDRESS);
        }

        // GET: CustomerAddress/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CUSTOMER_ADDRESS cUSTOMER_ADDRESS = db.CUSTOMER_ADDRESS.Find(id);
            if (cUSTOMER_ADDRESS == null)
            {
                return HttpNotFound();
            }
            ViewBag.CustomerID = new SelectList(db.CUSTOMERs, "CustomerID", "Email", cUSTOMER_ADDRESS.CustomerID);
            return View(cUSTOMER_ADDRESS);
        }

        // POST: CustomerAddress/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "CustomerID,AddressID,Street,City,State,Zipcode")] CUSTOMER_ADDRESS cUSTOMER_ADDRESS)
        {
            if (ModelState.IsValid)
            {
                db.Entry(cUSTOMER_ADDRESS).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.CustomerID = new SelectList(db.CUSTOMERs, "CustomerID", "Email", cUSTOMER_ADDRESS.CustomerID);
            return View(cUSTOMER_ADDRESS);
        }

        // GET: CustomerAddress/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CUSTOMER_ADDRESS cUSTOMER_ADDRESS = db.CUSTOMER_ADDRESS.Find(id);
            if (cUSTOMER_ADDRESS == null)
            {
                return HttpNotFound();
            }
            return View(cUSTOMER_ADDRESS);
        }

        // POST: CustomerAddress/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            CUSTOMER_ADDRESS cUSTOMER_ADDRESS = db.CUSTOMER_ADDRESS.Find(id);
            db.CUSTOMER_ADDRESS.Remove(cUSTOMER_ADDRESS);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
