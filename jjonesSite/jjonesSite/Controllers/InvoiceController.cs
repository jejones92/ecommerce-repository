﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using jjonesSite.Models;

namespace jjonesSite.Controllers
{
    public class InvoiceController : Controller
    {
        private jjonesEEntities db = new jjonesEEntities();

        // GET: Invoice
        public ActionResult Index()
        {
            var iNVOICEs = db.INVOICEs.Include(i => i.CUSTOMER);
            return View(iNVOICEs.ToList());
        }

        // GET: Invoice/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            INVOICE iNVOICE = db.INVOICEs.Find(id);
            if (iNVOICE == null)
            {
                return HttpNotFound();
            }
            return View(iNVOICE);
        }

        // GET: Invoice/Create
        public ActionResult Create()
        {
            ViewBag.CustomerID = new SelectList(db.CUSTOMERs, "CustomerID", "Email");
            return View();
        }

        // POST: Invoice/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "InvoiceID,InvoiceDate,CustomerID,NumberOfItems,OrderCost,InvoiceTotal")] INVOICE iNVOICE)
        {
            if (ModelState.IsValid)
            {
                db.INVOICEs.Add(iNVOICE);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.CustomerID = new SelectList(db.CUSTOMERs, "CustomerID", "Email", iNVOICE.CustomerID);
            return View(iNVOICE);
        }

        // GET: Invoice/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            INVOICE iNVOICE = db.INVOICEs.Find(id);
            if (iNVOICE == null)
            {
                return HttpNotFound();
            }
            ViewBag.CustomerID = new SelectList(db.CUSTOMERs, "CustomerID", "Email", iNVOICE.CustomerID);
            return View(iNVOICE);
        }

        // POST: Invoice/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "InvoiceID,InvoiceDate,CustomerID,NumberOfItems,OrderCost,InvoiceTotal")] INVOICE iNVOICE)
        {
            if (ModelState.IsValid)
            {
                db.Entry(iNVOICE).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.CustomerID = new SelectList(db.CUSTOMERs, "CustomerID", "Email", iNVOICE.CustomerID);
            return View(iNVOICE);
        }

        // GET: Invoice/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            INVOICE iNVOICE = db.INVOICEs.Find(id);
            if (iNVOICE == null)
            {
                return HttpNotFound();
            }
            return View(iNVOICE);
        }

        // POST: Invoice/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            INVOICE iNVOICE = db.INVOICEs.Find(id);
            db.INVOICEs.Remove(iNVOICE);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
