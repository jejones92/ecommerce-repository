//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace jjonesSite.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class CUSTOMER
    {
        public CUSTOMER()
        {
            this.CARTs = new HashSet<CART>();
            this.CREDIT_CARD = new HashSet<CREDIT_CARD>();
            this.CUSTOMER_ADDRESS = new HashSet<CUSTOMER_ADDRESS>();
            this.INVOICEs = new HashSet<INVOICE>();
        }
    
        public int CustomerID { get; set; }
        public string Email { get; set; }
        public string LastName { get; set; }
        public string FirstName { get; set; }
        public string HomePhone { get; set; }
        public string WorkPhone { get; set; }
        public string Password { get; set; }
    
        public virtual ICollection<CART> CARTs { get; set; }
        public virtual ICollection<CREDIT_CARD> CREDIT_CARD { get; set; }
        public virtual ICollection<CUSTOMER_ADDRESS> CUSTOMER_ADDRESS { get; set; }
        public virtual ICollection<INVOICE> INVOICEs { get; set; }
    }
}
