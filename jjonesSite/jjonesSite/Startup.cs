﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(jjonesSite.Startup))]
namespace jjonesSite
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
